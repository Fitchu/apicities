﻿using AngleSharp.Html.Dom;
using Parser.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class CityParser : IParser<string[]>
    {
        public string[] Parse(IHtmlDocument document)
        {
            var list = new List<string>();

            var countries = document.QuerySelectorAll("i").Where(item=>item.ClassName!=null && item.ClassName.Contains("fa flags flag-ab"));
            var cities = document.QuerySelectorAll("a").Where(item => item.ClassName != null && item.ClassName.Contains("link black"));

            foreach (var country in countries)
            {
                list.Add(country.TextContent);
                Console.WriteLine(country.TextContent);
            }

            return list.ToArray();

        }
    }
}
