﻿using Parser.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class ParserSettings : IParserSettings
    {
        public string BaseUrl { get; set; } = "https://www.gismeteo.ru";
        public string Prefix { get; set; } = "catalog";
    }
}
