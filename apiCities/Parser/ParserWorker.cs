﻿using AngleSharp.Html.Parser;
using Parser.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class ParserWorker<T> where T : class
    {
        IParser<T> parser;
        IParserSettings parserSettings;
        HtmlLoader loader;
        public IParser<T> Parser
        {
            get
            {
                return parser;
            }
            set
            {
                parser = value;
            }
        }


        public IParserSettings Settings
        {
            get
            {
                return parserSettings;
            }
            set
            {
                parserSettings = value;
                loader = new HtmlLoader(value);
            }
        }


        public ParserWorker(IParser<T> parser)
        {
            this.parser = parser;
        }

        public ParserWorker(IParser<T> parser, IParserSettings parserSetttings):this(parser)
        {
            this.parserSettings = parserSetttings;
            
        }

        public void Start()
        {
            Worker();
        }

        private async void Worker()
        {
            var source = await loader.GetSource();
            var domParser = new HtmlParser();
            var document = await domParser.ParseDocumentAsync(source);
            var result = parser.Parse(document);

            Console.WriteLine(result);
           
        }

    }
}
