﻿using apiCities.Models;
using Newtonsoft.Json;
using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Text;

namespace apiCities.Controllers
{
    public class CitiesController : ApiController
    {
        private CitiesDBEntities db = new CitiesDBEntities();


        [HttpGet]
        public string GetLess()
        {
            var Items = (from city in db.Cities2
                         select new
                         {
                             city.CityId,
                             city.CityName,
                             city.Country
                         });

            var json = JsonConvert.SerializeObject(Items);
            return json;
        }


        [HttpGet]
        public string Get()
        {
            var Items = (from city in db.Cities
                         select new
                         {
                             city.CityId,
                             city.CityName,
                             Country = city.Country.CountryName
                         });
            
            var json= JsonConvert.SerializeObject(Items);
            return json;
        }


        [HttpGet]
        public string Get(int id)
        {
          
            var itemgoals = db.Cities.Find(id).Goals.Select(e => new { e.GoalId, e.GoalName });

           
            var item =from city in db.Cities
                     
                      where city.CityId == id
                      select new
                      {
                          city.CityId,
                          city.CityName,
                          city.Country.CountryName,
                          city.Description
                      };


   
            string json=@"{"+String.Format(@"'СityId':{0},'CityName':'{1}','Country':'{2}','Description':'Какое-то описание города','Goals':[", 
                                        item.ToList()[0].CityId, 
                                        item.ToList()[0].CityName, 
                                        item.ToList()[0].CountryName
                                        );
            string goals = "";
            int i = 0;
            foreach(var g in itemgoals)
            {
                ++i;
                goals +=  @"{"+String.Format(@"'GoalId':{0},'GoalName':'{1}'", 
                                        g.GoalId, 
                                        g.GoalName
                                        )+@"}";

                if (i< itemgoals.Count())
                {
                    goals += @", ";
                }
                  
            }
            json += goals;

            json+=@"]";
            json += @"}";


            return json;
        }


        [Route("api/cities/{id}/goals")]
        [HttpGet]
        public string Goals(int id)
        {

            var Items = db.Cities.Find(id).Goals.Select(e => new { e.GoalId, e.GoalName });
            var json = JsonConvert.SerializeObject(Items, Formatting.Indented);
            return json;

        }



        [Route("api/cities/{id}/description")]
        [HttpGet]
        public string Description(int id)
        {

            var Items = db.Cities.Find(id).Description;
                 
            var json = JsonConvert.SerializeObject(Items);
            return json;

        }


        [Route("api/cities/{id}/weather")]
        [HttpGet]
        public string Weather(int id)
        {
           
             var tmp = from city in db.Cities
                       where city.CityId == id
                       select new {city.latitude,city.longitude,city.CityName};

            string url = String.Format("http://api.openweathermap.org/data/2.5/find?lat={0}&lon={1}&cnt=1&units=metric&appid=e7791d1bd3a53fc8be7b4f3285c34063", tmp.ToArray()[0].latitude, tmp.ToArray()[0].longitude);

            HttpWebRequest httpWorkerRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWorkerRequest.GetResponse();

            string response;

            using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                response = streamReader.ReadToEnd();
            }

            var pars = JObject.Parse(response);

             var weatherforecast = new WeatherData(pars);

            var json = JsonConvert.SerializeObject(weatherforecast);
            return json;
        }


        public class dataRange
        {
            public int id;
            public int startDate;
            public int endDate;
        }


        [HttpPost]
        public string Post(dataRange json)
        {
            var tmp = from city in db.Cities
                      where city.CityId == json.id
                      select new { city.latitude, city.longitude, city.CityName };


            DateTime startDate = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(json.startDate);

            DateTime endDate = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(json.endDate);

            int ras = (endDate.Date - startDate.Date).Days;
            
            var json2 = new ListWeatherRange(ras, startDate);
            var ans = JsonConvert.SerializeObject(json2.list);
            
            return ans;
        }


     
    }
}