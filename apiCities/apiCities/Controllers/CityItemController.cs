﻿using apiCities.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace apiCities.Controllers
{
    public class CityItemController : ApiController
    {
        private CitiesDBEntities db = new CitiesDBEntities();

        public class NameCity
        {
            public string Name;
        }


        [HttpPost]
        public string Post(NameCity name)
        {
            var item = from city in db.Cities
                       where city.CityName == name.Name
                       select new { cityId = city.CityId, city.CityName, city.Country.CountryName };

            var json = JsonConvert.SerializeObject(item);
            return json;
        }

    }
}