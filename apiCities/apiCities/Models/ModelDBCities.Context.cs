﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace apiCities.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class CitiesDBEntities : DbContext
    {
        public CitiesDBEntities()
            : base("name=CitiesDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>()
               .HasMany(p => p.Cities)
               .WithRequired(p => p.Country)
               .HasForeignKey(f => f.CountryId);
            modelBuilder.Entity<Cities>().HasMany(g => g.Goals).WithMany();
            modelBuilder.Entity<Goals>().HasMany(p => p.Cities).WithMany();
            base.OnModelCreating(modelBuilder);
        }
    
        public virtual DbSet<Cities> Cities { get; set; }
        public virtual DbSet<Cities2> Cities2 { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Goals> Goals { get; set; }
    }
}
