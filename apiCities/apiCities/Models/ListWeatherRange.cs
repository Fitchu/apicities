﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiCities.Models
{
    public class ListWeatherRange
    {
        public List<WeatherRange> list;
        public ListWeatherRange(int ras,DateTime startDate)
        {
            
            list = new List<WeatherRange>();
            for(int u= 0; u <= ras; u++)
            {
                
                list.Add(new WeatherRange(startDate.AddDays(u)));
            }

        }
    }
}