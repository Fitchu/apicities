﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiCities.Models
{
    public class WeatherRange
    {

        public string dt_txt;

        //weather
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }

        //wind
        public double speed { get; set; }

        //main
        public double temp { get; set; }
        public double pressure { get; set; }
        public int humidity { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
        public WeatherRange(DateTime dt) 
        {
          
                     
            dt_txt = dt.ToLongDateString();

            speed = Math.Round(new Random().NextDouble() * 30.0, 2);

            temp = Math.Round(new Random().NextDouble() * 45, 2);
            pressure = Math.Round(new Random().NextDouble()*960+1060, 2);
            humidity = new Random().Next(0, 100);
            temp_min = Math.Round(new Random().NextDouble() * 30.0, 2);
            temp_max = Math.Round(new Random().NextDouble() * 45.0, 2);

     
            main = "Clear";
            description = "clear sky";
            icon = "01n";
            
        }
    }
}