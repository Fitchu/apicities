﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace apiCities.Models
{
    public class WeatherData
    {
    

       
       
        public string dt_txt;
  
        //weather
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }

        //wind
        public double speed { get; set; }

        //main
        public double temp { get; set; }
        public double pressure { get; set; }
        public int humidity { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }

        public WeatherData(JObject pars)
        {
            dt_txt = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(Convert.ToInt32(pars["list"][0]["dt"].ToString())).ToLongDateString();
            main = pars["list"][0]["weather"][0]["main"].ToString();
            description = pars["list"][0]["weather"][0]["description"].ToString();
            icon = pars["list"][0]["weather"][0]["icon"].ToString();

            speed = Convert.ToDouble(pars["list"][0]["wind"]["speed"].ToString());

            temp= Convert.ToDouble(pars["list"][0]["main"]["temp"].ToString());
            pressure = Convert.ToDouble(pars["list"][0]["main"]["pressure"].ToString());
            humidity = Convert.ToInt32(pars["list"][0]["main"]["humidity"].ToString());
            temp_min = Convert.ToDouble(pars["list"][0]["main"]["temp_min"].ToString());
            temp_max = Convert.ToDouble(pars["list"][0]["main"]["temp_max"].ToString());

        }

    }
}