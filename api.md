# api-cities


# 
# 1. Получить список всех городов в базе [{CityId, Name, Ctr_Name}]
GET
/api/cities
#
[
  {
    "CityId": 6118,
    "CityName": "Омск",
    "Country": "Российская Федерация"
  },
  {
    "CityId": 6287,
    "CityName": "Владивосток",
    "Country": "Российская Федерация"
  },
  {
    "CityId": 6437,
    "CityName": "Москва",
    "Country": "Российская Федерация"
  },
  ...
  {
    "CityId": 10733,
    "CityName": "Москва",
    "Country": "Российская Федерация"
  },
  {
    "CityId": 10757,
    "CityName": "Сургут",
    "Country": "Российская Федерация"
  },
  {
    "CityId": 10795,
    "CityName": "Пермь",
    "Country": "Российская Федерация"
  }
]


# 
# 2. Получить всю информацию о городе
GET
/api/cities/{id} - id города
#
{
  "Goals": [
    {
      "GoalId": 1,
      "GoalName": "Отдых"
    },
    {
      "GoalId": 2,
      "GoalName": "Работа"
    },
    {
      "GoalId": 3,
      "GoalName": "Туризм"
    }
  ],
  "СityId": 1,
  "CityName": "Кейптаун",
  "Country": "Южно-Африканская Республика",
  "Description": "Какое-то описание города"
}



# 3. Получить прогноз погоды по id города на текущий момент
GET
/api/cities/{id}/weather - id города
#
{
  "dt_txt": "28 апреля 2019 г.",
  "main": "Clear",
  "description": "Sky is Clear",
  "icon": "01d",
  "speed": 2.1,
  "temp": 20.8,
  "pressure": 1013,
  "humidity": 44,
  "temp_min": 16.67,
  "temp_max": 25
}



# 4. Получить прогноз погоды для города между датами
POST
api/cities
# Тело запрсоа  json
{
   "id":8609,
   "startDate":1556436358,
   "endDate":1557041158 
}

# Ответ
[
  {
    "dt_txt": "28 апреля 2019 г.",
    "main": "Clear",
    "description": "clear sky",
    "icon": "01n",
    "speed": 23.28,
    "temp": 34.93,
    "pressure": 1037,
    "humidity": 77,
    "temp_min": 23.28,
    "temp_max": 34.93
  },
  {
    "dt_txt": "29 апреля 2019 г.",
    "main": "Clear",
    "description": "clear sky",
    "icon": "01n",
    "speed": 23.28,
    "temp": 34.93,
    "pressure": 1037,
    "humidity": 77,
    "temp_min": 23.28,
    "temp_max": 34.93
  },
  {
    "dt_txt": "30 апреля 2019 г.",
    "main": "Clear",
    "description": "clear sky",
    "icon": "01n",
    "speed": 23.28,
    "temp": 34.93,
    "pressure": 1037,
    "humidity": 77,
    "temp_min": 23.28,
    "temp_max": 34.93
  },
  {
    "dt_txt": "1 мая 2019 г.",
    "main": "Clear",
    "description": "clear sky",
    "icon": "01n",
    "speed": 23.28,
    "temp": 34.93,
    "pressure": 1037,
    "humidity": 77,
    "temp_min": 23.28,
    "temp_max": 34.93
  },
  {
    "dt_txt": "2 мая 2019 г.",
    "main": "Clear",
    "description": "clear sky",
    "icon": "01n",
    "speed": 23.28,
    "temp": 34.93,
    "pressure": 1037,
    "humidity": 77,
    "temp_min": 23.28,
    "temp_max": 34.93
  },
  {
    "dt_txt": "3 мая 2019 г.",
    "main": "Clear",
    "description": "clear sky",
    "icon": "01n",
    "speed": 23.28,
    "temp": 34.93,
    "pressure": 1037,
    "humidity": 77,
    "temp_min": 23.28,
    "temp_max": 34.93
  },
  {
    "dt_txt": "4 мая 2019 г.",
    "main": "Clear",
    "description": "clear sky",
    "icon": "01n",
    "speed": 23.28,
    "temp": 34.93,
    "pressure": 1037,
    "humidity": 77,
    "temp_min": 23.28,
    "temp_max": 34.93
  },
  {
    "dt_txt": "5 мая 2019 г.",
    "main": "Clear",
    "description": "clear sky",
    "icon": "01n",
    "speed": 23.28,
    "temp": 34.93,
    "pressure": 1037,
    "humidity": 77,
    "temp_min": 23.28,
    "temp_max": 34.93
  }
]
# 
# 5. GET "api/cities/{name}/cityid"
[
  {
    "cityId": 6437,
    "CityName": "Москва",
    "CountryName": "Российская Федерация"
  },
  {
    "cityId": 35170,
    "CityName": "Москва",
    "CountryName": "Нидерланды"
  }
    
]
